import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import React, { Component } from "react";
import Navbar from "./components/Navbar/Navbar";
import TaskView from "./components/TaskView/TaskView";

class App extends Component {
    constructor(props) {
        super(props);
        this.handleChangeScreen = this.handleChangeScreen.bind(this);
    }
    state = {
        selected: "tasks"
    };

    handleChangeScreen = screen => {
        this.setState({ selected: screen });
    };
    render() {
        return (
            <main className="App">
                <header className="App-header">
                    <Navbar onChange={this.handleChangeScreen} />
                </header>
                <TaskView />
            </main>
        );
    }
}

export default App;
