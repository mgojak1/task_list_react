import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import App from "./App";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Signup from "./components/Signup/Signup";
import Login from "./components/Login/Login";
ReactDOM.render(
    <Router>
        {" "}
        <Route exact path="/" component={Login} />
        <Route path="/signup" component={Signup} />
        <Route path="/tasks" component={App} />
    </Router>,
    document.getElementById("root")
);
serviceWorker.unregister();
