import React, { Component } from "react";
import "./login.css";
import FontAwesome from "react-fontawesome";

class Login extends Component {
    state = {
        alert: false
    };
    handleSubmit() {
        if (!this.validateForm()) {
            this.passwordInput.value = "";
            this.setState({ alert: true });
        } else {
            this.setState({ alert: false });
            window.location = "/tasks";
        }
    }

    validateForm() {
        let validated = true;
        if (this.emailInput.value !== "admin@admin") validated = false;
        if (this.passwordInput.value !== "12345678") validated = false;
        return validated;
    }
    render() {
        return (
            <div className="loginWrapper">
                <div className="loginDialog">
                    <h5 className="loginHeader">Log In</h5>
                    <form className="loginForm">
                        <label htmlFor="email">Email: </label>
                        <input
                            type="text"
                            name="email"
                            className="loginInputEmail"
                            placeholder="Email"
                            ref={input => {
                                this.emailInput = input;
                            }}
                        />
                        <label htmlFor="password">Password: </label>
                        <input
                            type="password"
                            name="password"
                            className="loginInputPassword"
                            placeholder="Password"
                            ref={input => {
                                this.passwordInput = input;
                            }}
                        />
                        <button
                            className="loginSubmit"
                            onClick={e => {
                                this.handleSubmit();
                                e.preventDefault();
                            }}
                        >
                            Log In
                        </button>
                        <p
                            className="loginSignup"
                            onClick={() => {
                                window.location = "/signup";
                            }}
                        >
                            No Account? Sign Up
                        </p>
                        {this.state.alert ? (
                            <div className="loginAlert">
                                <span>Wrong username or password!</span>
                                <span
                                    className="loginAlertClose"
                                    onClick={() => {
                                        this.setState({ alert: false });
                                    }}
                                >
                                    <FontAwesome name="times" />
                                </span>
                            </div>
                        ) : (
                            ""
                        )}
                    </form>
                </div>
            </div>
        );
    }
}

export default Login;
