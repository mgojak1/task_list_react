import React, { Component } from "react";
import "./navbar.css";
import FontAwesome from "react-fontawesome";

class Navbar extends Component {
    constructor(props) {
        super(props);
        this.handleSearchInput = this.handleSearchInput.bind(this);
    }
    state = {
        search: ""
    };

    handleSearchInput(event) {
        this.setState({ search: event.target.value });
    }

    render() {
        return (
            <div className="navbar">
                <ul className="navList">
                    <li
                        className={
                            "navItem" +
                            (this.state.selected === "tasks" ? " selected" : "")
                        }
                    >
                        Task List
                    </li>
                    <li className="searchItem">
                        <input
                            type="text"
                            className="searchBox"
                            placeholder="Search tasks"
                            value={this.state.search}
                            ref={input => {
                                this.searchInput = input;
                            }}
                            onChange={this.handleSearchInput}
                        />
                        <span
                            className={
                                "searchClear" +
                                (this.state.search !== "" ? "" : " hidden")
                            }
                            onClick={() => {
                                this.setState({ search: "" });
                            }}
                        >
                            <FontAwesome name="times" />
                        </span>
                    </li>
                    <li
                        className="navItem logout"
                        onClick={() => {
                            window.location = "/";
                        }}
                    >
                        Logout
                    </li>
                </ul>
            </div>
        );
    }
}

export default Navbar;
