class Palette {
    static dark0 = "#2E3440";
    static dark1 = "#3B4252";
    static dark2 = "#434C5E";
    static dark3 = "#4C566A";
    static light0 = "#D8DEE9";
    static light1 = "#E5E9F0";
    static light2 = "#ECEFF4";
    static blue0 = "#8FBCBB";
    static blue1 = "#88C0D0";
    static blue2 = "#81A1C1";
    static blue3 = "#5E81AC";
    static red = "#BF616A";
    static orange = "#D08770";
    static yellow = "#EBCB8B";
    static green = "#A3BE8C";
    static purple = "#B48EAD";

    static random = () => {
        let rand = Math.floor(Math.random() * Math.floor(9));
        if (rand === 0) return this.blue0;
        if (rand === 1) return this.blue1;
        if (rand === 2) return this.blue2;
        if (rand === 3) return this.blue3;
        if (rand === 4) return this.red;
        if (rand === 5) return this.orange;
        if (rand === 6) return this.yellow;
        if (rand === 7) return this.green;
        if (rand === 8) return this.purple;
    };
}

export default Palette;
