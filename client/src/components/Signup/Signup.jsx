import React, { Component } from "react";
import "./signup.css";

class Signup extends Component {
    state = {
        validEmail: true,
        emailPlaceholder: "Email",
        validPassword: true,
        passwordPlaceholder: "Password",
        validConfirmPassword: true,
        confirmPasswordPlaceholder: "Confirm password"
    };

    componentDidMount() {
        this.emailInput.addEventListener("input", e => {
            this.validateForm();
        });
        this.passwordInput.addEventListener("input", e => {
            this.validateForm();
        });
        this.passwordConfirmInput.addEventListener("input", e => {
            this.validateForm();
        });
    }

    componentWillUnmount() {
        this.emailInput.removeEventListener("input");
        this.passwordInput.removeEventListener("input");
        this.passwordConfirmInput.removeEventListener("input");
    }

    handleSubmit() {
        if (!this.validateForm()) {
            this.passwordInput.value = "";
            this.passwordConfirmInput.value = "";
        } else window.location = "/";
    }

    validateForm() {
        let validated = true;
        if (
            !this.emailInput.value.includes("@") ||
            this.emailInput.value.length < 3
        ) {
            this.setState({
                validEmail: false,
                emailPlaceholder: "Not a valid email"
            });
            validated = false;
        } else {
            this.setState({ validEmail: true, emailPlaceholder: "Email" });
        }
        if (this.passwordInput.value.length < 8) {
            this.setState({
                validPassword: false,
                passwordPlaceholder: "Password too short"
            });
            validated = false;
        } else {
            this.setState({
                validPassword: true,
                passwordPlaceholder: "Password"
            });
        }
        if (
            this.passwordInput.value !== this.passwordConfirmInput.value ||
            this.passwordConfirmInput.value.length < 8
        ) {
            this.setState({
                validConfirmPassword: false,
                confirmPasswordPlaceholder: "Not matching password"
            });
            validated = false;
        } else {
            this.setState({
                validConfirmPassword: true,
                confirmPasswordPlaceholder: "Confirm password"
            });
        }
        return validated;
    }
    render() {
        return (
            <div className="signupWrapper">
                <div className="signupDialog">
                    <h5 className="signupHeader">Sign Up</h5>
                    <form className="signupForm">
                        <label htmlFor="email">Email: </label>
                        <input
                            type="text"
                            name="email"
                            className={
                                "signupInputEmail" +
                                (!this.state.validEmail ? " signupInvalid" : "")
                            }
                            placeholder={this.state.emailPlaceholder}
                            ref={input => {
                                this.emailInput = input;
                            }}
                        />
                        <label htmlFor="password">Password: </label>
                        <input
                            type="password"
                            name="password"
                            className={
                                "signupInputPassword" +
                                (!this.state.validPassword
                                    ? " signupInvalid"
                                    : "")
                            }
                            placeholder={this.state.passwordPlaceholder}
                            ref={input => {
                                this.passwordInput = input;
                            }}
                        />
                        <label htmlFor="confirmPassword">
                            Confirm Password:{" "}
                        </label>
                        <input
                            type="password"
                            name="confirmPassword"
                            className={
                                "signupInputConfirmPassword" +
                                (!this.state.validConfirmPassword
                                    ? " signupInvalid"
                                    : "")
                            }
                            placeholder={this.state.confirmPasswordPlaceholder}
                            ref={input => {
                                this.passwordConfirmInput = input;
                            }}
                        />
                        <button
                            className="signupSubmit"
                            onClick={e => {
                                this.handleSubmit();
                                e.preventDefault();
                            }}
                        >
                            Sign Up
                        </button>
                        <p
                            className="loginSignup"
                            onClick={() => {
                                window.location = "/";
                            }}
                        >
                            Have an Account? Log In
                        </p>
                    </form>
                </div>
            </div>
        );
    }
}

export default Signup;
