import React, { Component } from "react";
import "./badge.css";

class Badge extends Component {
    render() {
        return (
            <span
                className="badge"
                style={{ backgroundColor: this.props.color }}
            >
                {this.props.text}
            </span>
        );
    }
}

export default Badge;
