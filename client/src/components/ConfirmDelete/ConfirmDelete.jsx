import React, { Component } from "react";
import "./confirmDelete.css";

class ConfirmDelete extends Component {
    state = {};
    render() {
        return (
            <div className="confirmDeleteWrapper">
                <div className="confirmDeleteDialog">
                    <h4 className="confirmDeleteHeader">
                        {this.props.name} will be deleted.
                    </h4>
                    <button className="confirmDeleteConfirm">Delete</button>
                    <button className="confirmDeleteCancel">Cancel</button>
                </div>
            </div>
        );
    }
}

export default ConfirmDelete;
