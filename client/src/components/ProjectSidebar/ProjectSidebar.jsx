import React, { Component } from "react";
import "./projectSidebar.css";
import Palette from "../Palette";

class ProjectSidebar extends Component {
    constructor(props) {
        super(props);
        this.handleChangeProject = this.handleChangeProject.bind(this);
        this.handleEnter = this.handleEnter.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    state = {
        name: ""
    };

    getNextID() {
        let id = 2;
        this.props.projects.forEach(project => {
            if (project.id > id) id = project.id;
        });
        return id + 1;
    }

    createProject = () => {
        let name = this.state.name;
        if (name.length > 20) name = name.slice(0, 20);

        const project = {
            name: name,
            deletable: true,
            color: Palette.random(),
            tasks: [],
            id: this.getNextID()
        };
        this.props.onAdd(project);
    };

    handleChangeProject(id) {
        this.props.onChange(id);
    }

    handleEnter(event) {
        if (event.key === "Enter" || event.type === "click") {
            if (this.state.name !== "") {
                this.setState({ name: "" });
                this.createProject();
            }
        }
    }

    handleChange(event) {
        this.setState({ name: event.target.value });
    }

    render() {
        const projectList = this.props.projects.map(project => (
            <li
                className={
                    "projectItem" +
                    (project.id === this.props.selected
                        ? " selectedProject"
                        : "")
                }
                onClick={() => this.handleChangeProject(project.id)}
                key={project.id}
                style={
                    project.id === this.props.selected
                        ? {
                              color: project.color,
                              borderRight: "4px solid " + project.color
                          }
                        : {}
                }
            >
                {project.name}
                <span className="projectCount">{project.tasks.length}</span>
            </li>
        ));
        return (
            <div className="projectSidebar">
                <ul className="projectList">{projectList}</ul>
                <div className="addProjectDiv">
                    <input
                        type="text"
                        className="addProjectInput"
                        placeholder="Add new project"
                        onKeyPress={this.handleEnter}
                        onChange={this.handleChange}
                        value={this.state.name}
                    ></input>
                    <span
                        className="projectCheckAdd"
                        onClick={this.handleEnter}
                    >
                        +
                    </span>
                </div>
            </div>
        );
    }
}
export default ProjectSidebar;
