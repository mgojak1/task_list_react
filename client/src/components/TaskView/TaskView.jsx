import React, { Component } from "react";
import ProjectSidebar from "../ProjectSidebar/ProjectSidebar";
import Tasks from "../Tasks/Tasks";
import Palette from "../Palette";
import "./taskView.css";
import Dropdown from "../Dropdown/Dropdown";
import TaskSidebar from "../TaskSidebar/TaskSidebar";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

class TaskView extends Component {
    constructor(props) {
        super(props);
        this.handleChangeProject = this.handleChangeProject.bind(this);
        this.handleAddTask = this.handleAddTask.bind(this);
        this.handleAddProject = this.handleAddProject.bind(this);
        this.handleDeleteTask = this.handleDeleteTask.bind(this);
        this.handleEditTask = this.handleEditTask.bind(this);
        this.handleDeleteProject = this.handleDeleteProject.bind(this);
        this.handleShowTaskSidebar = this.handleShowTaskSidebar.bind(this);
    }
    state = {
        projects: [
            {
                id: 0,
                deletable: false,
                name: "Today",
                color: Palette.blue2,
                tasks: [
                    {
                        id: 2,
                        task: "Return book to library",
                        priority: 4,
                        date: "2020-03-02",
                        tags: [{ name: "Today", color: Palette.blue2 }]
                    },
                    {
                        id: 3,
                        task: "Pay bills",
                        priority: 2,
                        date: "2020-01-12",
                        tags: [{ name: "Today", color: Palette.blue2 }]
                    }
                ]
            },
            {
                id: 1,
                name: "Week",
                deletable: false,
                color: Palette.green,
                tasks: [
                    {
                        id: 4,
                        task: "Finish homework",
                        priority: 6,
                        date: "2020-03-02",
                        tags: [{ name: "Week", color: Palette.green }]
                    },
                    {
                        id: 5,
                        task: "Study for exams",
                        priority: 1,
                        date: "2020-01-12",
                        tags: [{ name: "Week", color: Palette.green }]
                    },
                    {
                        id: 6,
                        task: "Visit museum",
                        priority: 1,
                        date: "2020-11-12",
                        tags: [{ name: "Week", color: Palette.green }]
                    }
                ]
            },
            {
                id: 4,
                name: "Shopping",
                deletable: true,
                color: Palette.orange,
                tasks: [
                    {
                        id: 0,
                        task: "Buy milk",
                        priority: 3,
                        date: "2020-03-02",
                        tags: [{ name: "Shopping", color: Palette.orange }]
                    },
                    {
                        id: 1,
                        task: "Buy notebook",
                        priority: 4,
                        date: "2020-12-12",
                        tags: [{ name: "Shopping", color: Palette.orange }]
                    }
                ]
            }
        ],
        currentProject: 0,
        currentTask: {},
        showTaskSidebar: false,
        modalShow: false
    };

    handleAddTask = (id, newTask) => {
        this.state.projects
            .find(project => {
                return project.id === id;
            })
            .tasks.push(newTask);
        this.setState(state => ({ projects: state.projects }));
    };

    handleAddProject = newProject => {
        const projectList = this.state.projects.concat(newProject);
        this.setState({
            projects: projectList,
            currentProject: newProject.id
        });
    };

    handleDeleteTask = (projectId, taskId) => {
        let projectList = this.state.projects;
        let i = projectList.findIndex(project => {
            return project.id === projectId;
        });
        projectList[i].tasks = projectList[i].tasks.filter(task => {
            return task.id !== taskId;
        });
        this.setState({ projects: projectList });
        if (this.state.currentTask.id === taskId) {
            if (projectList[i].tasks.length !== 0)
                this.setState({ currentTask: projectList[i].tasks[0] });
            else this.setState({ currentTask: {} });
        }
    };

    handleChangeProject = id => {
        this.setState(state => ({
            currentProject: id,
            projects: state.projects
        }));

        const current = this.state.projects.find(project => {
            return project.id === id;
        });

        if (current.tasks.length > 0) {
            this.setState({ currentTask: current.tasks[0] });
        } else {
            this.setState({ currentTask: {}, showTaskSidebar: false });
        }
    };

    handleDeleteProject = () => {
        const projectList = this.state.projects.filter(project => {
            return project.id !== this.state.currentProject;
        });
        this.setState({
            projects: projectList,
            currentProject: 0
        });
    };

    getTaskById(id) {
        return this.state.projects
            .find(project => {
                return project.id === this.state.currentProject;
            })
            .tasks.find(task => {
                return task.id === id;
            });
    }

    handleShowTaskSidebar = id => {
        this.setState({
            showTaskSidebar: true,
            currentTask: this.getTaskById(id)
        });
    };

    handleEditTask = editedTask => {
        let projectList = this.state.projects;
        let i = projectList.findIndex(project => {
            return project.id === this.state.currentProject;
        });

        let j = projectList[i].tasks.findIndex(task => {
            return task.id === editedTask.id;
        });

        projectList[i].tasks[j] = editedTask;
        this.setState({
            projects: projectList
        });
    };

    MyVerticallyCenteredModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Modal heading
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Centered Modal</h4>
                    <p>
                        Cras mattis consectetur purus sit amet fermentum. Cras
                        justo odio, dapibus ac facilisis in, egestas eget quam.
                        Morbi leo risus, porta ac consectetur ac, vestibulum at
                        eros.
                    </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    render() {
        return (
            <div
                className={
                    "taskViewContainer" +
                    (this.state.showTaskSidebar &&
                    JSON.stringify(this.state.currentTask) !==
                        JSON.stringify({})
                        ? ""
                        : " taskViewContainerHide")
                }
            >
                <div className="sidebar">
                    <ProjectSidebar
                        projects={this.state.projects}
                        onChange={this.handleChangeProject}
                        onAdd={this.handleAddProject}
                        selected={this.state.currentProject}
                    />
                </div>
                <div className="taskViewList">
                    <div className="taskListProjectName">
                        <span className="taskListHeader">
                            {
                                this.state.projects.find(project => {
                                    return (
                                        project.id === this.state.currentProject
                                    );
                                }).name
                            }
                        </span>
                        {this.state.projects.find(project => {
                            return project.id === this.state.currentProject;
                        }).deletable ? (
                            <span className="menuIcon">
                                <Dropdown
                                    onDelete={() =>
                                        this.setState({
                                            modalShow: true
                                        })
                                    }
                                />
                            </span>
                        ) : (
                            ""
                        )}
                    </div>
                    <Tasks
                        project={this.state.projects.find(project => {
                            return project.id === this.state.currentProject;
                        })}
                        onAdd={this.handleAddTask}
                        onDelete={this.handleDeleteTask}
                        onClick={this.handleShowTaskSidebar}
                        selected={this.state.currentTask}
                    />
                </div>
                <div
                    className={
                        "taskViewtaskSidebar" +
                        (this.state.showTaskSidebar &&
                        JSON.stringify(this.state.currentTask) !==
                            JSON.stringify({})
                            ? ""
                            : " taskSidebarHide")
                    }
                >
                    <TaskSidebar
                        project={this.state.projects.find(project => {
                            return project.id === this.state.currentProject;
                        })}
                        task={this.state.currentTask}
                        onDelete={this.handleDeleteTask}
                        onEdit={this.handleEditTask}
                        onClose={() => {
                            this.setState({ showTaskSidebar: false });
                        }}
                    />
                </div>
                {this.state.modalShow ? (
                    <Modal
                        show={true}
                        size="md"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                        style={{ fontFamily: "Rubik" }}
                    >
                        <Modal.Body>
                            <h5>
                                Deleting list "
                                {
                                    this.state.projects.find(project => {
                                        return (
                                            project.id ===
                                            this.state.currentProject
                                        );
                                    }).name
                                }
                                "
                            </h5>
                            <p>This action is permament.</p>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button
                                variant="danger"
                                onClick={() => {
                                    this.handleDeleteProject();
                                    this.setState({ modalShow: false });
                                }}
                            >
                                Delete
                            </Button>
                            <Button
                                onClick={() => {
                                    this.setState({ modalShow: false });
                                }}
                            >
                                Close
                            </Button>
                        </Modal.Footer>
                    </Modal>
                ) : (
                    ""
                )}
            </div>
        );
    }
}

export default TaskView;
