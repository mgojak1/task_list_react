import React, { Component } from "react";
import "./taskSidebar.css";
import FontAwesome from "react-fontawesome";
import Palette from "../Palette";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
class TaskSidebar extends Component {
    constructor(props) {
        super(props);
        this.handleDeleteTask = this.handleDeleteTask.bind(this);
        this.handleRenameTask = this.handleRenameTask.bind(this);
        this.handleChangePriority = this.handleChangePriority.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
    }
    state = {
        modalShow: false
    };

    handleDeleteTask = id => {
        this.props.onDelete(this.props.project.id, id);
    };

    handleRenameTask(e) {
        this.props.task.task = e.target.value;
        this.props.onEdit(this.props.task);
    }

    handleChangePriority(e) {
        this.props.task.priority = e.target.value;
        this.props.onEdit(this.props.task);
    }

    handleChangeDate(e) {
        this.props.task.date = e.target.value;
        this.props.onEdit(this.props.task);
    }
    render() {
        return (
            <div className="taskSidebar">
                <ul className="taskSidebarList">
                    <li
                        className="taskSidebarItem"
                        onClick={() => this.props.onClose()}
                    >
                        Close sidebar
                        <span className="trashIcon">
                            <FontAwesome name="times" />
                        </span>
                    </li>
                    <div className="taskSidebarRename">
                        <input
                            type="text"
                            className="taskSidebarRenameInput"
                            placeholder="Rename"
                            onKeyPress={this.handleEnter}
                            onChange={e => {
                                this.handleRenameTask(e);
                            }}
                            value={this.props.task.task || ""}
                        ></input>
                    </div>
                    <li className="taskSidebarItem">
                        Priority:{" "}
                        <span className="selectPrioritySpan">
                            <select
                                name="selectPriority"
                                className="selectPriority"
                                onChange={e => {
                                    this.handleChangePriority(e);
                                }}
                                onClick={e => {
                                    this.handleChangePriority(e);
                                }}
                                value={this.props.task.priority}
                            >
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                            </select>
                        </span>
                    </li>
                    <li className="taskSidebarItem">
                        Deadline:
                        <input
                            type="date"
                            className="taskSidebarDate"
                            min="2020-01-01"
                            value={this.props.task.date || "2020-01-01"}
                            onChange={e => {
                                this.handleChangeDate(e);
                            }}
                        />
                    </li>
                    <li
                        className="taskSidebarItem"
                        style={{ color: Palette.red }}
                        onClick={() => this.setState({ modalShow: true })}
                    >
                        Delete task
                        <span className="trashIcon">
                            <FontAwesome name="trash" />
                        </span>
                    </li>
                </ul>
                {this.state.modalShow ? (
                    <Modal
                        show={true}
                        size="md"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                        style={{ fontFamily: "Rubik" }}
                    >
                        <Modal.Body>
                            <h5>Deleting task "{this.props.task.task}"</h5>
                            <p>This action is permament.</p>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button
                                variant="danger"
                                onClick={() => {
                                    this.handleDeleteTask(this.props.task.id);
                                    this.setState({ modalShow: false });
                                }}
                            >
                                Delete
                            </Button>
                            <Button
                                onClick={() => {
                                    this.setState({ modalShow: false });
                                }}
                            >
                                Close
                            </Button>
                        </Modal.Footer>
                    </Modal>
                ) : (
                    ""
                )}
            </div>
        );
    }
}

export default TaskSidebar;
