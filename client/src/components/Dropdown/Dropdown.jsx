import React, { Component } from "react";
import FontAwesome from "react-fontawesome";
import "./dropdown.css";
import Palette from "../Palette";

class Dropdown extends Component {
    constructor(props) {
        super(props);
        this.handleShowDropdown = this.handleShowDropdown.bind(this);
        this.handleHideDropdown = this.handleHideDropdown.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    state = {
        show: false
    };
    handleShowDropdown() {
        this.setState(state => ({ show: !state.show }));
    }

    handleHideDropdown() {
        this.setState({ show: false });
        console.log("BLUR");
    }

    handleClick = e => {
        if (this.nodeDiv.contains(e.target)) {
            return;
        }
        this.setState({ show: false });
    };

    componentDidMount() {
        document.addEventListener("mousedown", this.handleClick);
    }
    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClick);
    }

    render() {
        return (
            <div
                className="dropdown"
                ref={node => {
                    this.nodeDiv = node;
                }}
                title="More options"
            >
                <span
                    className="dropdownButton"
                    onClick={this.handleShowDropdown}
                    onBlur={this.handleHideDropdown}
                >
                    <FontAwesome name="ellipsis-h" />
                </span>
                <div
                    className={
                        "dropdownContent" +
                        (this.state.show === true ? " showDropdown" : "")
                    }
                >
                    <ul className="dropdownList">
                        <li
                            style={{ color: Palette.red }}
                            onClick={() => this.props.onDelete()}
                        >
                            Delete List
                            <span className="trashIcon">
                                <FontAwesome name="trash" />
                            </span>
                        </li>
                        <li>Rename List</li>
                        <li>Change Color</li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Dropdown;
