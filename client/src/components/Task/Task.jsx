import React, { Component } from "react";
import Badge from "../Badge/Badge";
import "./task.css";

class Task extends Component {
    constructor(props) {
        super(props);
        this.handleCheck = this.handleCheck.bind(this);
        this.state = {
            isChecked: false
        };
    }

    handleCheck = () => {
        this.checkbox.checked = true;
        this.setState({ isChecked: true });
        this.checkbox.setAttribute("disabled", "");
        setTimeout(() => {
            this.props.onDelete(this.props.id);
        }, 600);
    };

    render() {
        const badgeList = this.props.tags.map(tag => (
            <li className="badgeItem" key={tag.name}>
                <Badge text={tag.name} color={tag.color} />
            </li>
        ));
        return (
            <div
                className="task"
                onClick={() => this.props.onClick(this.props.id)}
                style={
                    this.props.id === this.props.selected.id
                        ? {
                              backgroundColor: "#eceff4"
                          }
                        : {}
                }
            >
                <span className="taskCheck" title="Complete task">
                    <label className="checkLabel" onClick={this.handleCheck}>
                        <input
                            type="checkbox"
                            ref={input => {
                                this.checkbox = input;
                            }}
                        />
                        <span></span>
                    </label>
                </span>
                <span
                    className={
                        "taskName" + (this.state.isChecked ? " crossed" : "")
                    }
                >
                    {this.props.task}
                </span>
                <span className="taskPriority">{this.props.priority}</span>
                <span className="taskDate">{this.props.date}</span>
                <ul className="badgeList">{badgeList}</ul>
            </div>
        );
    }
}

export default Task;
