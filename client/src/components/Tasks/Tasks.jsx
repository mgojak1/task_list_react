import React, { Component } from "react";
import Task from "../Task/Task";
import AddTask from "../AddTask/AddTask";
import TaskHeader from "../TaskHeader/TaskHeader";

class Tasks extends Component {
    constructor(props) {
        super(props);
        this.handleAddTask = this.handleAddTask.bind(this);
        this.handleDeleteTask = this.handleDeleteTask.bind(this);
        this.handleSort = this.handleSort.bind(this);
    }
    componentDidMount() {
        const sortedTasks = this.props.project.tasks.sort((t1, t2) => {
            return t1.task > t2.task ? 1 : -1;
        });
        this.setState({ tasks: sortedTasks });
    }

    getNextID() {
        let id = 0;
        this.props.project.tasks.forEach(task => {
            if (task.id > id) id = task.id;
        });
        return id + 1;
    }

    handleAddTask = newTask => {
        newTask["id"] = this.getNextID();
        this.props.onAdd(this.props.project.id, newTask);
    };

    handleDeleteTask = id => {
        this.props.onDelete(this.props.project.id, id);
    };

    handleSort = sortBy => {
        if (sortBy === "task") {
            const sortedTasks = this.props.project.tasks.sort((t1, t2) => {
                return t1.task > t2.task ? 1 : -1;
            });
            this.setState({ tasks: sortedTasks });
        } else if (sortBy === "priority") {
            const sortedTasks = this.props.project.tasks.sort((t1, t2) => {
                return t1.priority > t2.priority ? 1 : -1;
            });
            this.setState({ tasks: sortedTasks });
        } else if (sortBy === "date") {
            const sortedTasks = this.props.project.tasks.sort((t1, t2) => {
                return t1.date > t2.date ? 1 : -1;
            });
            this.setState({ tasks: sortedTasks });
        }
    };

    render() {
        const taskList = this.props.project.tasks.map(task => (
            <Task
                task={task.task}
                tags={task.tags}
                priority={task.priority}
                date={task.date}
                id={task.id}
                key={task.id}
                onDelete={this.handleDeleteTask}
                onClick={this.props.onClick}
                selected={this.props.selected}
            />
        ));

        return (
            <div>
                <TaskHeader sortBy={this.handleSort} />
                <div>{taskList}</div>
                <AddTask
                    addTask={this.handleAddTask}
                    project={this.props.project.name}
                    color={this.props.project.color}
                />
            </div>
        );
    }
}

export default Tasks;
