import React, { Component } from "react";
import FontAwesome from "react-fontawesome";
import "./taskHeader.css";

class TaskHeader extends Component {
    constructor(props) {
        super(props);
        this.handleSortName = this.handleSortName.bind(this);
        this.handleSortPriority = this.handleSortPriority.bind(this);
        this.handleSortDate = this.handleSortDate.bind(this);
    }

    state = {
        taskIcon: "",
        priorityIcon: "",
        dateIcon: ""
    };

    componentDidMount() {
        this.setState({
            taskIcon: this.addIcon(),
            priorityIcon: "",
            dateIcon: ""
        });
    }

    addIcon = () => {
        return (
            <span className="sortIcon">
                <FontAwesome name="sort-down" />
            </span>
        );
    };

    handleSort = sortBy => {
        this.props.sortBy(sortBy);
    };

    handleSortName() {
        this.setState({
            taskIcon: this.addIcon(),
            priorityIcon: "",
            dateIcon: ""
        });
        this.handleSort("task");
    }

    handleSortPriority() {
        this.setState({
            taskIcon: "",
            priorityIcon: this.addIcon(),
            dateIcon: ""
        });
        this.handleSort("priority");
    }

    handleSortDate() {
        this.setState({
            taskIcon: "",
            priorityIcon: "",
            dateIcon: this.addIcon()
        });
        this.handleSort("date");
    }

    render() {
        return (
            <div className="taskHeader">
                <span
                    className="taskHeaderDescription"
                    onClick={this.handleSortName}
                >
                    Task{this.state.taskIcon}
                </span>
                <span
                    className="taskHeaderPriority"
                    onClick={this.handleSortPriority}
                >
                    Priority{this.state.priorityIcon}
                </span>
                <span
                    className="taskHeaderDeadline"
                    onClick={this.handleSortDate}
                >
                    Deadline{this.state.dateIcon}
                </span>
                <span className="headerProjectList">Tags</span>
            </div>
        );
    }
}

export default TaskHeader;
