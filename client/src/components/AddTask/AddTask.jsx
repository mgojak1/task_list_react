import React, { Component } from "react";
import "./addTask.css";

class AddTask extends Component {
    constructor(props) {
        super(props);
        this.state = { name: "" };
        this.handleChange = this.handleChange.bind(this);
        this.handleEnter = this.handleEnter.bind(this);
        this.handleLeave = this.handleLeave.bind(this);
        this.handleFocusEnter = this.handleFocusEnter.bind(this);
        this.handleFocusLeave = this.handleFocusLeave.bind(this);
    }

    createTask = () => {
        const task = {
            task: this.state.name,
            priority: "",
            date: "",
            tags: [{ name: this.props.project, color: this.props.color }]
        };
        this.props.addTask(task);
    };

    handleChange(event) {
        this.setState({ name: event.target.value });
    }

    handleFocusEnter() {
        //this.nameInput.focus();
    }

    handleFocusLeave() {
        /*setTimeout(() => {
            if (this.state.name === "") this.nameInput.blur();
        }, 100);*/
    }

    handleEnter(event) {
        if (event.key === "Enter" || event.type === "click") {
            if (this.state.name !== "") {
                this.createTask();
                this.setState({ name: "" });
            }
        }
    }

    handleLeave() {
        //this.setState({ name: "" });
    }

    render() {
        return (
            <div
                className="addTask"
                onMouseEnter={this.handleFocusEnter}
                onMouseLeave={this.handleFocusLeave}
            >
                <span className="taskCheckAdd" onClick={this.handleEnter}>
                    +
                </span>
                <input
                    ref={input => {
                        this.nameInput = input;
                    }}
                    className="taskNameAdd"
                    type="text"
                    placeholder="Add new task"
                    value={this.state.name}
                    onChange={this.handleChange}
                    onKeyPress={this.handleEnter}
                    onBlur={this.handleLeave}
                />
            </div>
        );
    }
}

export default AddTask;
